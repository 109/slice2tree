package slice2tree

import (
	"encoding/json"
)

type Node struct {
	ID       int     `json:"id"`
	Depth    int     `json:"depth"`
	Label    string  `json:"label"`
	Children []*Node `json:"children"`
}
type Key struct {
	Label string
	depth int
	ID    int
}

type Tree struct {
	Nodes []*Node
	mp    map[Key]*Node
}

func NewTree() *Tree {
	return &Tree{mp: make(map[Key]*Node)}
}

func (t *Tree) Import(nodes []Node) {
	for i, n := range nodes {
		var node Node
		node = n
		var k = Key{Label: node.Label, depth: i, ID: node.ID}
		node.Depth = i
		if _, ok := t.mp[k]; !ok {
			if i == 0 {
				t.Nodes = append(t.Nodes, &node)
			} else {
				fatherNode := t.mp[Key{Label: nodes[i-1].Label, depth: i - 1, ID: nodes[i-1].ID}]
				fatherNode.Children = append(fatherNode.Children, &node)
			}

			t.mp[k] = &node
		}
	}
}

func (t *Tree) AddChilrenById(id int, n Node) {
	for k, v := range t.mp {
		if k.ID == id {
			v.Children = append(v.Children, &n)
		}
	}
}

func (t *Tree) Json() ([]byte, error) {
	return json.Marshal(t.Nodes)
}
