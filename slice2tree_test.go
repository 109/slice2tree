package slice2tree

import (
	"testing"
)

func TestImport(t *testing.T) {
	n1 := Node{ID: 1, Label: "111"}
	n2 := Node{ID: 2, Label: "222"}
	n3 := Node{ID: 3, Label: "333"}
	var ns []Node
	ns = append(ns, n1, n2, n3)
	t.Log(ns)
	tree := NewTree()
	tree.Import(ns)
	n1 = Node{ID: 1, Label: "111"}
	n2 = Node{ID: 2, Label: "222"}
	n3 = Node{ID: 4, Label: "444"}
	n4 := Node{ID: 5, Label: "99999999"}
	var ns2 []Node
	ns2 = append(ns2, n1, n2, n3)
	tree.Import(ns2)

	t.Log("root node:", tree.Nodes[0])
	tree.AddChilrenById(4, n4)

	if dat, err := tree.Json(); err != nil {
		t.Fail()
		t.Log(err)
	} else {
		t.Log(string(dat))
	}
}
