# slice2tree

For element UI tree view

`````````package main

import (
        s2t "gitlab.com/109/slice2tree"
        "log"
)

func main() {
        n1 := s2t.Node{ID: 1, Label: "111"}
        n2 := s2t.Node{ID: 2, Label: "222"}
        n3 := s2t.Node{ID: 3, Label: "333"}
        var ns []s2t.Node
        ns = append(ns, n1, n2, n3)
        log.Println(ns)
        tree := s2t.NewTree()
        tree.Import(ns)
        n1 = s2t.Node{ID: 1, Label: "111"}
        n2 = s2t.Node{ID: 2, Label: "222"}
        n3 = s2t.Node{ID: 4, Label: "444"}
        var ns2 []s2t.Node
        ns2 = append(ns2, n1, n2, n3)
        tree.Import(ns2)

        log.Println("root node:", tree.Nodes[0])

        if dat, err := tree.Json(); err != nil {
                log.Println(err)
        } else {
                log.Println(string(dat))
        }
}
`````````````
